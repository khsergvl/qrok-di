package com.test.app;

import com.framework.di.annotations.Repository;


@Repository
public interface ImagesRepository {
	String findByDiscription();
	String findByAuthorName(String authorName);
}
