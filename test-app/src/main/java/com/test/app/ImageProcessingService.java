package com.test.app;

import com.framework.di.annotations.Autowired;
import com.framework.di.annotations.Component;

import java.util.Optional;


@Component
public class ImageProcessingService {
	private String a;
	private String b;

	@Autowired
	private ImagesRepository imagesRepository;

	@Autowired
	private NotExistedService notExistedService;

	public void processMyImage(){

		imagesRepository.findByDiscription();
		String testInfo = imagesRepository.findByAuthorName("Alex");
		System.out.println("Result returned by method call is " + testInfo);
		if (Optional.ofNullable(notExistedService).isPresent()) {
			notExistedService.callPhone();
		} else {
			System.out.println("notExistedService doesn't exist");
		}
	}
}
