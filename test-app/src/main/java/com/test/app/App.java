package com.test.app;


import com.framework.di.common.ApplicationContext;
import com.framework.di.common.Context;

public class App
{
	public static void main( String[] args )
	{

		Context context = new ApplicationContext();
		ImageProcessingService service = (ImageProcessingService) context.getBean(ImageProcessingService.class);
		service.processMyImage();

	}
}
