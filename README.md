QROK DI Framework
============

    This is small Dependency Injection framework,
    build to simplify App development process.

Framework common feature - scan classpath for annotated classes,
instantiate them as beans, and inject to annotated fields.
To simplify DI just annotate class with public empty constructor with
@Component annotation - it will be automatically pushed to App context.
Framework provides dynamic interface implementation for interfaces marked as
@Repository. Bean fields marked as @Autowired will be autowired with beans from context

Test App included, so just:
Clone repository. Execute in bash

cd qrok-di && mvn install && mvn exec:java -pl test-app

Features
========

- @Component, @Repository  bean inject;
- @Component field autowiring;
- Dynamic method generation for @Repository methods that consists "FindBy"  words in the method name;
- Covered with Unit tests;
- Logo.



Requirements
--------------------
- git
- maven
- java 8



License
=======

GNU General Public License
