package com.framework.di.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Marker for component classes that should be passed to context.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Component {
}
