package com.framework.di.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Marker for autowired fields.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Autowired {
}
