package com.framework.di.common;

/**
 * Interface for bean processing.
 */
public interface BeanProcessor {
	Object createNewInstance(Class type);
	String getSupportedAnnotationName();
}
