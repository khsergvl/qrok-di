package com.framework.di.common;

/**
 * Interface for bean post processing after initialization.
 */
public interface BeanPostProcessor {
	Object postConstruct(Object bean);
	String getSupportedAnnotationName();
}
