package com.framework.di.common;

import com.framework.di.annotations.Component;

/**
 * This class implements logic for component class instantiations.
 */
public class ComponentBeanProcessor implements BeanProcessor {
	private final String supportedAnnotationName =  Component.class.getName();

	/**
	 * This method produces bean - requested class instance,
	 * for component annotated non-abstract class with public empty constructor only.
	 * @param type
	 *          Type for instantiation.
	 *
	 * @return annotation name String representation.
	 */
	@Override
	public Object createNewInstance(Class type) {
		try {
			return type.getConstructor().newInstance();
		} catch (Exception e) {
			System.out.println("Unsupported component type or public empty constructor doesn't exist - "  + type.getName());
			return null;
		}
	}

	/**
	 * This method produces supported annotation name.
	 *
	 * @return annotation name String representation.
	 */
	@Override
	public String getSupportedAnnotationName() {
		return supportedAnnotationName;
	}
}
