package com.framework.di.common;

/**
 * DI Context interface.
 */
public interface Context {
	Object getBean(Class clazz);
}
