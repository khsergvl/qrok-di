package com.framework.di.common;

import com.framework.di.annotations.Autowired;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Optional;

/**
 * This class implements logic for component's field post processing - bean injecting (autowiring).
 */
public class AutowiredBeanPostProcessor implements BeanPostProcessor {
	private Map<Class, Object> beanRegistry;

	private final String supportedAnnotationName = Autowired.class.getName();

	public AutowiredBeanPostProcessor(Map<Class, Object> beanRegistry) {
		this.beanRegistry = beanRegistry;
	}

	/**
	 * This method injects bean to components fields
	 *
	 * @param bean
	 *        Bean for processing.
	 *
	 * @return Bean after field inject.
	 */
	@Override
	public Object postConstruct(Object bean) {
		Field[] fields = bean.getClass().getDeclaredFields();
		for (Field field : fields) {
			Autowired annotation = field.getAnnotation(Autowired.class);
			Optional.ofNullable(annotation).ifPresent(a -> {
				field.setAccessible(true);
				Type fieldType = field.getGenericType();
				try {
					field.set(bean, beanRegistry.get(fieldType));
				} catch (Exception e) {
					System.out.println("Unable to autowire field " + field.getName());
				}
			});
		}
		return bean;
	}

	/**
	 * This method produces supported annotation name.
	 *
	 * @return annotation name String representation.
	 */
	@Override
	public String getSupportedAnnotationName() {
		return supportedAnnotationName;
	}
}
