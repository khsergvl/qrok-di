package com.framework.di.common;

import java.util.Map;

/**
 * Interface for bean factory.
 */
public interface BeanFactory {
	Map<Class, Object> buildBeanRegistry();
}
