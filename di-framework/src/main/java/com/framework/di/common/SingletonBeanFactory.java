package com.framework.di.common;


import java.lang.annotation.Annotation;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This class implements bean registry build logic.
 */
public class SingletonBeanFactory implements BeanFactory {

	public SingletonBeanFactory(Map<Class, Object> beanRegistry) {
		this.beanRegistry = beanRegistry;
	}

	private Map<Class, Object> beanRegistry;
	private final BeanProcessor componentBeanProcessor = new ComponentBeanProcessor();
	private final BeanProcessor repositoryBeanProcessor = new RepositoryBeanProcessor();

	public SingletonBeanFactory() {
		this.build();
	}


	/**
	 * This method scan classpath for classes marked with supported annotation.
	 *
	 * @param supportedAnnotations
	 *         Set of Strings with supported annotation name.
	 *
	 * @return Class List marked with annotation.
	 */
	private List<Class<?>> scanClassPathForAnnotatedClasses(Set<String> supportedAnnotations) {

		List<Class<?>> classList = new ArrayList<>();



			URL classPathForScan = SingletonBeanFactory.class.getClassLoader().getResource("");
			Optional.ofNullable(classPathForScan).ifPresent(c -> {
				String stringClassPathForScan = c.getPath();
				try {
					Files.find(Paths.get(stringClassPathForScan), 999, (p, bfa) -> bfa.isRegularFile() && p.getFileName().toString().matches(".*\\.class"))
						.forEach(path -> {
							try {
								String filePath = path.toString();
								String className = filePath.substring(stringClassPathForScan.length(), filePath.indexOf(".class")).replace('/', '.');
								Class<?> repoClass = Class.forName(className);
								Annotation[] annotations = repoClass.getAnnotations();

								for (Annotation annotation : annotations) {
									if (supportedAnnotations.contains(annotation.annotationType().getName())) {
										classList.add(repoClass);
									}
								}
							} catch(Exception e){
								e.printStackTrace();
							}
						});
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		return classList;
	}

	/**
	 * This method build bean registry.
	 */
	private void build() {
		beanRegistry = new ConcurrentHashMap();
		Set<String> supportedAnnotation = new HashSet<>();
		supportedAnnotation.add(componentBeanProcessor.getSupportedAnnotationName());
		supportedAnnotation.add(repositoryBeanProcessor.getSupportedAnnotationName());

		List<Class<?>> classList = scanClassPathForAnnotatedClasses(supportedAnnotation);
		classList.stream().forEach(implementationClass -> {
			Annotation[] annotations = implementationClass.getAnnotations();
			for (Annotation annotation : annotations) {

				if (componentBeanProcessor.getSupportedAnnotationName().equals(annotation.annotationType().getName())) {
					Object bean = componentBeanProcessor.createNewInstance(implementationClass);
					Optional.ofNullable(bean).ifPresent(b -> {
						beanRegistry.put(implementationClass, b);
						for (Class interfaCe : implementationClass.getInterfaces()) {
							beanRegistry.put(interfaCe, b);
						}
					});
				}

				if (repositoryBeanProcessor.getSupportedAnnotationName().equals(annotation.annotationType().getName())) {
					Object bean = repositoryBeanProcessor.createNewInstance(implementationClass);
					Optional.ofNullable(bean).ifPresent(b -> beanRegistry.putIfAbsent(implementationClass, b));
				}
			}

		});

	}

	/**
	 * This method produces access to bean registry.
	 *
	 * @return HashMap with
	 *            K - Class based on which bean has been instantiated,
	 *            V - ready bean instance
	 */
	@Override
	public Map<Class, Object> buildBeanRegistry() {
		return beanRegistry;
	}
}
