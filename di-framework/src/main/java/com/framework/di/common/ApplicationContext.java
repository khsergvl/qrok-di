package com.framework.di.common;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

/**
 * This class implements logic for application context initialization.
 */
public class ApplicationContext implements Context {

	private Map<Class, Object> beanRegistry;

	public ApplicationContext (){

		InputStream inputStream = ApplicationContext.class.getClassLoader().getResourceAsStream("logo.info");
		String readLine;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))){
			while (((readLine = br.readLine()) != null)) {
				System.out.println(readLine);
			}
		} catch (Exception e) {
			System.out.println("Qrok DI framework");
		}

		BeanFactory beanFactory = new SingletonBeanFactory();
		beanRegistry = beanFactory.buildBeanRegistry();
		BeanPostProcessor beanPostProcessor = new AutowiredBeanPostProcessor(beanRegistry);
		beanRegistry.forEach((clazz, bean) -> bean = beanPostProcessor.postConstruct(bean));

	}

	public ApplicationContext(Map<Class, Object> beanRegistry) {
		this.beanRegistry = beanRegistry;
	}

	/**
	 * This method produces access to application context.
	 *
	 * @param clazz
	 *        instantiated class
	 *
	 * @return requested bean
	 */
	@Override
	public Object getBean(Class clazz) {
		return beanRegistry.get(clazz);
	}
}
