package com.framework.di.common;

import com.framework.di.annotations.Repository;

import java.lang.reflect.Proxy;
import java.util.Optional;

/**
 * This class implements logic for dynamic interface implementation.
 */
public class RepositoryBeanProcessor implements BeanProcessor {
	private final String supportedAnnotationName = Repository.class.getName();

	/**
	 * This method produces bean - dynamic interface implementation,
	 * for repository annotated interfaces.
	 * @param type
	 *          Type for implementation.
	 *
	 * @return annotation name String representation.
	 */

	@Override
	public Object createNewInstance(Class type) {
		if (type.isInterface()) {
			return Proxy.newProxyInstance(type.getClassLoader(), new Class[]{ type }, (proxy, method, args) -> {
				if (method.getName().contains("findBy")) {
					if (Optional.ofNullable(args).isPresent()) {
						System.out.println(method.getName() + args[0].toString());
						return method.getName() + args[0].toString();
					} else {
						System.out.println(method.getName());
						return method.getName();
					}
				} else return new String();
			});
		} else {
			System.out.println("Unsupported component type - repository must be an interface");
			return null;
		}
	}


	/**
	 * This method produces supported annotation name.
	 *
	 * @return annotation name String representation.
	 */
	@Override
	public String getSupportedAnnotationName() {
		return supportedAnnotationName;
	}

}
