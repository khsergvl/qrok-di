package com.framework.di;


import com.framework.di.annotations.Component;
import com.framework.di.common.BeanProcessor;
import com.framework.di.common.ComponentBeanProcessor;
import com.framework.di.config.TestService;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ComponentBeanProcessorTest {
	private BeanProcessor beanProcessor;

	@Before
	public void setupUp() {
		beanProcessor = new ComponentBeanProcessor();
	}

	@Test
	public void testGetSupportedAnnotationName() throws Exception {

		assertEquals(beanProcessor.getSupportedAnnotationName(), Component.class.getName());

	}

	@Test
	public void testCreateNewInstance() throws Exception {

		com.framework.di.config.TestService bean = (com.framework.di.config.TestService) beanProcessor.createNewInstance(TestService.class);
		assertNotNull(bean);
		assertTrue(bean instanceof com.framework.di.config.TestService);

	}

}
