package com.framework.di;

import com.framework.di.common.ApplicationContext;
import com.framework.di.common.Context;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ApplicationContextTest {
	private Context applicationContext;
	private Map<Class, Object> beanRegistry;
	private Class testClass;

	@Before
	public void setupUp() {

		testClass = com.framework.di.config.TestService.class;
		beanRegistry =  new ConcurrentHashMap();
		try {
			beanRegistry.put(testClass, testClass.getConstructor().newInstance());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		applicationContext = new ApplicationContext(beanRegistry);

	}

	@Test
	public void testGetBean() throws Exception {

		assertTrue(applicationContext.getBean(testClass) instanceof com.framework.di.config.TestService);

	}

}
