package com.framework.di;

import com.framework.di.common.BeanFactory;
import com.framework.di.common.SingletonBeanFactory;
import org.junit.Before;
import org.junit.Test;


import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SingletonBeanFactoryTest {
	private BeanFactory beanFactory;

	@Before
	public void setupUp() {

		beanFactory = new SingletonBeanFactory(new ConcurrentHashMap());

	}

	@Test
	public void testBuildBeanRegistry() throws Exception {

		assertNotNull(beanFactory.buildBeanRegistry());
		assertTrue(beanFactory.buildBeanRegistry() instanceof ConcurrentHashMap);
		assertTrue(beanFactory.buildBeanRegistry().isEmpty());

	}

}
