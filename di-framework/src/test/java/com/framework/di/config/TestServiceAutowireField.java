package com.framework.di.config;

import com.framework.di.annotations.Autowired;
import com.framework.di.annotations.Component;

@Component
public class TestServiceAutowireField {
	@Autowired
	private TestService testService;

	public TestService getTestService() {
		return testService;
	}
}
