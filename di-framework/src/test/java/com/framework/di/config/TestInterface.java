package com.framework.di.config;

import com.framework.di.annotations.Repository;

/**
 * Created by sergey on 2/25/18.
 */
@Repository
public interface TestInterface {
	String findByName();
	String findByLastName (String lastName);
	String call();
}
