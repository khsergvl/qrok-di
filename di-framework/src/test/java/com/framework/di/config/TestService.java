package com.framework.di.config;

import com.framework.di.annotations.Component;

/**
 * Created by sergey on 2/25/18.
 */
@Component
public class TestService {
	public void sayQuote(){}
}
