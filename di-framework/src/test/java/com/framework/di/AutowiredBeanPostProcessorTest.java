package com.framework.di;


import com.framework.di.annotations.Autowired;
import com.framework.di.common.AutowiredBeanPostProcessor;
import com.framework.di.common.BeanPostProcessor;
import com.framework.di.common.BeanProcessor;
import com.framework.di.common.ComponentBeanProcessor;
import com.framework.di.config.TestService;
import com.framework.di.config.TestServiceAutowireField;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.*;

public class AutowiredBeanPostProcessorTest {
	private BeanPostProcessor beanPostProcessor;
	private BeanProcessor beanProcessor;
	private Map<Class,Object> beanRegistry;
	private TestServiceAutowireField bean;


	@Before
	public void setupUp() {
		beanProcessor = new ComponentBeanProcessor();
		beanRegistry = new ConcurrentHashMap<>();

		beanRegistry.put(TestService.class, beanProcessor.createNewInstance(TestService.class));

		bean = (TestServiceAutowireField) beanProcessor.createNewInstance(TestServiceAutowireField.class);
		beanRegistry.put(TestServiceAutowireField.class, bean);

		beanPostProcessor = new AutowiredBeanPostProcessor(beanRegistry);

	}

	@Test
	public void testGetSupportedAnnotationName() throws Exception {

		assertEquals(beanPostProcessor.getSupportedAnnotationName(), Autowired.class.getName());

	}

	@Test
	public void testPostConstruct() throws Exception {

		assertNull(bean.getTestService());
		bean = (TestServiceAutowireField) beanPostProcessor.postConstruct(bean);
		assertNotNull(bean);
		assertTrue(bean instanceof TestServiceAutowireField);
		assertNotNull(bean.getTestService());
		assertTrue(bean.getTestService() instanceof TestService);
		assertSame(bean.getTestService(), beanRegistry.get(TestService.class));

	}

}
