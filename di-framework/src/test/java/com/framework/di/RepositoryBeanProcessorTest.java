package com.framework.di;


import com.framework.di.annotations.Repository;
import com.framework.di.common.BeanProcessor;
import com.framework.di.common.RepositoryBeanProcessor;
import com.framework.di.config.TestInterface;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class RepositoryBeanProcessorTest {
	private BeanProcessor beanProcessor;

	@Before
	public void setupUp() {
		beanProcessor = new RepositoryBeanProcessor();
	}

	@Test
	public void testGetSupportedAnnotationName() throws Exception {

		assertEquals(beanProcessor.getSupportedAnnotationName(), Repository.class.getName());

	}

	@Test
	public void testCreateNewInstance() throws Exception {

		com.framework.di.config.TestInterface bean = (com.framework.di.config.TestInterface) beanProcessor.createNewInstance(TestInterface.class);
		assertNotNull(bean);
		assertTrue(bean instanceof TestInterface);
		assertEquals(bean.findByName(), "findByName");
		assertEquals(bean.findByLastName("Khomich"), "findByLastNameKhomich");
		Class beanClass = bean.getClass();
		Method methods[] = beanClass.getMethods();

		for (Method method : methods) {
			if (method.getName().contains("findBy") && method.getParameters().length == 0) {
				assertEquals(method.getName(),  method.invoke(bean, new Object[]{}));
			}
			if (method.getName().contains("findBy") && method.getParameters().length != 0) {
				assertEquals(method.getName() + "Khomich", method.invoke(bean, new Object[]{"Khomich"}));
			}
		}

	}

}
